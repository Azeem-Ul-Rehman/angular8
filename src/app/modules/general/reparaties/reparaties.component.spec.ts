import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReparatiesComponent } from './reparaties.component';

describe('ReparatiesComponent', () => {
  let component: ReparatiesComponent;
  let fixture: ComponentFixture<ReparatiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReparatiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReparatiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
